package com.profile.demo;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.profile.demo.business.PruebaBusiness;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Test1 {
	
	@Autowired
	private PruebaBusiness pruebaBusiness;
	
	@Before
	public void setUp() {		
	}
	
	@Test
	public void esNotNull() {
		assertNotNull(pruebaBusiness.getRandom());
	}
	
	@Test
	public void esEntreValoresCorrectos() {
		assertTrue(pruebaBusiness.getRandom() >= 1 && pruebaBusiness.getRandom() <= 60);
	}
	
}
