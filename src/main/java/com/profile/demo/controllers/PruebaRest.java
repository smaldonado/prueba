package com.profile.demo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiResponses;

import io.swagger.annotations.ApiResponse;

import com.profile.demo.beans.Prueba;
import com.profile.demo.business.PruebaBusiness;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("root")
public class PruebaRest {

	@Autowired private PruebaBusiness pruebaBusiness;
	@Autowired private Prueba prueba;
	
	@ApiOperation(value = "get customer", response = String.class)
	
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Random recivido", response = String.class),
			@ApiResponse(code = 500, message = "Internal Server Error"),
			@ApiResponse(code = 404, message = "Not found") }
	)
	@GetMapping("/random")
	public String pruebaGet() {
		String strValue = String.valueOf(pruebaBusiness.getRandom());		
		return strValue;
	}
	
	@GetMapping("/varias")
	public String pruebasVarias() {
		String str = prueba.prueba();		
		return str;
	}
}
