package com.profile.demo.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import lombok.Getter;
import lombok.Setter;

@ConfigurationProperties(prefix = "mail")
@PropertySource("classpath:configprop.properties")
@Configuration
@Getter
@Setter
public class ConfigPropertiesHost {
	private String host;
    private int port;
    private String from;    
}
	