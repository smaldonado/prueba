package com.profile.demo.business;

import java.util.Random;

import org.springframework.stereotype.Service;

@Service
public final class PruebaBusiness {

	public int getRandom() {
		Random rand = new Random();
		int numRand = rand.nextInt(60) + 1; //random entre 1 y 60

		return numRand;
	}
}
