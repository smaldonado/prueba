package com.profile.demo.beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.profile.demo.properties.ConfigPropertiesDirecc;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Component
public final class Prueba {

	@Autowired
	private ConfigPropertiesDirecc direcc;

	public String prueba() {
		String str = direcc.getTipo();
		return str;
	}

}
