package com.profile.demo.beans;

import java.util.ArrayList;
import java.util.List;

public final class PruebaConsole {

	@SuppressWarnings("serial")
	public static void main(String[] args) {
		List<String> strList = new ArrayList<String>() {
			{
				add("uno");
				add("dos");
				add("tres");
			}
		};
		
		String str = String.join(",", strList);
		System.out.println(str);
	}
}
