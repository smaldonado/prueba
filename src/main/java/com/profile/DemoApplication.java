package com.profile;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import de.codecentric.boot.admin.client.registration.Application;

@SpringBootApplication
public class DemoApplication {

	private static ConfigurableApplicationContext ctx;

	public static void main(String[] args) {
		ctx = SpringApplication.run(DemoApplication.class, args);
	}

	public static void restart() {
		ApplicationArguments args = ctx.getBean(ApplicationArguments.class);

		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				ctx.close();
				ctx = SpringApplication.run(Application.class, args.getSourceArgs());
			}
		});

		thread.setDaemon(false);
		thread.start();
	}
}
